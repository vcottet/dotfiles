local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ %s)"

#PROMPT='${ret_status}%{$fg_bold[green]%}%p %{$fg[cyan]%}%c %{$fg_bold[blue]%}$(git_prompt_info)%{$fg_bold[blue]%} % %{$reset_color%}'
PROMPT='%{$fg_bold[green]%}%T %{$fg[cyan]%}%5c ${ret_status} %{$reset_color%}'

ZSH_THEME_GIT_PROMPT_PREFIX=""
ZSH_THEME_GIT_PROMPT_SUFFIX=""
ZSH_THEME_GIT_PROMPT_SEPARATOR=""
ZSH_THEME_GIT_PROMPT_BRANCH="%{$fg_bold[cyan]%}%{⎇  %3G%}"
ZSH_THEME_GIT_PROMPT_STAGED="%{$fg[green]%}%{ ●%2G%}"
ZSH_THEME_GIT_PROMPT_CONFLICTS="%{$fg[red]%}%{ ✖%2G%}"
ZSH_THEME_GIT_PROMPT_CHANGED="%{$fg[red]%}%{ ✚%2G%}"
ZSH_THEME_GIT_PROMPT_BEHIND="%{ ↓%2G%}"
ZSH_THEME_GIT_PROMPT_AHEAD="%{ ↑%2G%}"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{ …%2G%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg_bold[green]%}%{ ✔%2G%}"

RPROMPT='$(git_super_status)'
