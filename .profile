# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.

DEV_HOME="$HOME/Dev"
TOOLS_HOME="$DEV_HOME/Tools"
PATH="$DEV_HOME/bin:$TOOLS_HOME/bin:$PATH"
export DEV_HOME TOOLS_HOME

. $TOOLS_HOME/anaconda3/etc/profile.d/conda.sh

# oracle
#ORACLE_HOME="/usr/lib/oracle/11.2/client64"
#ORACLE_SID="XE"
#TNS_ADMIN="$HOME/.oracle"
#NLS_LANG="FRENCH_FRANCE.AL32UTF8"
#export ORACLE_HOME ORACLE_SID TNS_ADMIN NLS_LANG

EDITOR="vim"
export EDITOR

GIT_EDITOR="vim"
export GIT_EDITOR

export IBUS_ENABLE_SYNC_MODE=1

export PATH

# Don't check mail when opening terminal.
unset MAILCHECK

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# nvm
[[ -s "$HOME/.nvm/nvm.sh" ]] && source "$HOME/.nvm/nvm.sh"

# sdkman
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" && -z $(which sdkman-init.sh | grep '/sdkman-init.sh') ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
